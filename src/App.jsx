import { useState } from 'react'
import './App.css'
import './styles/media.css'
import IndexPage from './Pages/index'


function App() {
  return (
    <div className="App">
      <IndexPage />
    </div>
  )
}

export default App
