import React, { useState, useEffect } from 'react'
import logo from '../Header/img/Group 31.svg'
import burger from '../Header/img/burger.svg'

const Header = () => {
      const [fixed, setFixed] = useState(false)

      useEffect(() => {
            window.addEventListener('scroll', () => {
                  if (window.scrollY > 790) {
                        setFixed(true)  // 100 fixed header 
                  } else {
                        setFixed(false)
                  }
            })
      })

      const reloadingPages = () => {
            window.location.reload()
      }
      return (
            <header className={`header ${fixed && "fixedNav"}`}>
                  <div className="conatiner">
                        <div className='right_contaent'>
                              <div className='logo'>
                                    <img onClick={reloadingPages} src={logo} alt="" />
                              </div>

                              <div className='link_pages'>
                                    <ul>
                                          <li><a href="#home">HOME</a></li>
                                          <li><a href="#about">ABOUT</a></li>
                                          <li><a href="#portfolio">PORTFOLIO</a></li>
                                          <li><a href="#servicer">SERVICES</a></li>
                                          <li><a href="#blog">BLOCK</a></li>
                                          <li><a href="#contact">CONTACT</a></li>
                                    </ul>

                              </div>
                        </div>


                        <div className="_languages">
                              <div className="change_lan">
                                    <a href="#en">EN</a>
                                    <div className="line"></div>
                                    <a href="#ru">RU</a>
                              </div>
                        </div>
                        <div className="burger_menu">
                              <img src={burger} alt="" />
                        </div>
                  </div>
            </header>
      )
}
export default Header