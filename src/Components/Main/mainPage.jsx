import React from 'react'
import Header from '../Header/header'
import { StyledEngineProvider } from '@mui/material/styles';
import ReactDOM from 'react-dom';
import { Button } from '@mui/material';
import { Tooltip } from '@mui/material';
import { Box } from '@mui/material';
import line from '../Main/img/line.svg'
import MyImg from '../Main/img/My project 1.svg'



const MainPage = () => {

      return (
            <div className="bg_wrapper">
                  <div id='home'>
                        <Header />
                        <div className="conatiner">
                              <div className="main_wrapper">
                                    <div className='left_text_wrap_info_me'>
                                          <h1 className='name'>Gulyamov <span>Akbar</span></h1>
                                          <img className='line_down' src={line} alt="" />
                                          <h1 className='skils_info'>Entrepreneur,<span> Frontend Developer</span> and<br /> Lifelong Learner</h1>
                                          <div className="btn_info_more">
                                                <Tooltip title="Contact Me">
                                                      <Button variant="contained" >CONTACT</Button>
                                                </Tooltip>
                                          </div>
                                    </div>
                              </div>

                        </div>
                  </div>
            </div >
      )
}

export default MainPage